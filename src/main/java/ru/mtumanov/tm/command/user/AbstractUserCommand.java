package ru.mtumanov.tm.command.user;

import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(final User user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
