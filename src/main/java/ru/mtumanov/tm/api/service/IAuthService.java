package ru.mtumanov.tm.api.service;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

public interface IAuthService {

    User registry(String login, String password, String email) throws AbstractException;

    void login(String login, String password) throws AbstractException;

    void logout();

    boolean isAuth();

    String getUserId() throws AbstractException;

    User getUser() throws AbstractException;

}
