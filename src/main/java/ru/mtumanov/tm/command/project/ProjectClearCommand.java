package ru.mtumanov.tm.command.project;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectService().clear();
    }
    
}
