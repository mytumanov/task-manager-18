package ru.mtumanov.tm.command.task;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Remove task by index";
    }

    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().removeByIndex(index);
    }
    
}
