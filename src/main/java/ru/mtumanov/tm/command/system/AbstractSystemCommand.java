package ru.mtumanov.tm.command.system;

import ru.mtumanov.tm.api.service.ICommandService;
import ru.mtumanov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }
}
