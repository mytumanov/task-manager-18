package ru.mtumanov.tm.component;

import ru.mtumanov.tm.api.repository.ICommandRepository;
import ru.mtumanov.tm.api.repository.IProjectRepository;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.api.service.IAuthService;
import ru.mtumanov.tm.api.service.ICommandService;
import ru.mtumanov.tm.api.service.ILoggerService;
import ru.mtumanov.tm.api.service.IProjectService;
import ru.mtumanov.tm.api.service.IProjectTaskService;
import ru.mtumanov.tm.api.service.IServiceLocator;
import ru.mtumanov.tm.api.service.ITaskService;
import ru.mtumanov.tm.api.service.IUserService;
import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.command.project.ProjectChangeStatusByIdCommand;
import ru.mtumanov.tm.command.project.ProjectChangeStatusByIndexCommand;
import ru.mtumanov.tm.command.project.ProjectClearCommand;
import ru.mtumanov.tm.command.project.ProjectCompleteByIdCommand;
import ru.mtumanov.tm.command.project.ProjectCompleteByIndexCommand;
import ru.mtumanov.tm.command.project.ProjectCreateCommand;
import ru.mtumanov.tm.command.project.ProjectListCommand;
import ru.mtumanov.tm.command.project.ProjectRemoveByIdCommand;
import ru.mtumanov.tm.command.project.ProjectRemoveByIndexCommand;
import ru.mtumanov.tm.command.project.ProjectShowByIdCommand;
import ru.mtumanov.tm.command.project.ProjectShowByIndexCommand;
import ru.mtumanov.tm.command.project.ProjectStartByIdCommand;
import ru.mtumanov.tm.command.project.ProjectStartByIndexCommnd;
import ru.mtumanov.tm.command.project.ProjectUpdateByIdCommand;
import ru.mtumanov.tm.command.project.ProjectUpdateByIndexCommand;
import ru.mtumanov.tm.command.system.ApplicationAboutCommand;
import ru.mtumanov.tm.command.system.ApplicationExitCommand;
import ru.mtumanov.tm.command.system.ApplicationHelpCommand;
import ru.mtumanov.tm.command.system.ApplicationVersionCommand;
import ru.mtumanov.tm.command.system.ArgumentListCommand;
import ru.mtumanov.tm.command.system.CommandListCommand;
import ru.mtumanov.tm.command.system.SystemInfoCommand;
import ru.mtumanov.tm.command.task.TaskBindToProjectCommand;
import ru.mtumanov.tm.command.task.TaskChangeStatusByIdCommand;
import ru.mtumanov.tm.command.task.TaskChangeStatusByIndexCommand;
import ru.mtumanov.tm.command.task.TaskClearCommand;
import ru.mtumanov.tm.command.task.TaskCompleteByIdCommand;
import ru.mtumanov.tm.command.task.TaskCompleteByIndexCommand;
import ru.mtumanov.tm.command.task.TaskCreateCommand;
import ru.mtumanov.tm.command.task.TaskListCommand;
import ru.mtumanov.tm.command.task.TaskRemoveByIdCommand;
import ru.mtumanov.tm.command.task.TaskRemoveByIndexCommand;
import ru.mtumanov.tm.command.task.TaskShowByIdCommand;
import ru.mtumanov.tm.command.task.TaskShowByIndexCommand;
import ru.mtumanov.tm.command.task.TaskShowByProjectIdCommand;
import ru.mtumanov.tm.command.task.TaskStartByIdCommand;
import ru.mtumanov.tm.command.task.TaskStartByIndexCommand;
import ru.mtumanov.tm.command.task.TaskUnbindFromProjectCommand;
import ru.mtumanov.tm.command.task.TaskUpdateById;
import ru.mtumanov.tm.command.task.TaskUpdateByIndex;
import ru.mtumanov.tm.command.user.UserChangePasswordCommand;
import ru.mtumanov.tm.command.user.UserLoginCommand;
import ru.mtumanov.tm.command.user.UserLogoutCommand;
import ru.mtumanov.tm.command.user.UserRegistryCommand;
import ru.mtumanov.tm.command.user.UserUpdateProfileCommand;
import ru.mtumanov.tm.command.user.UserViewProfileCommand;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.system.ArgumentNotSupportedException;
import ru.mtumanov.tm.exception.system.CommandNotSupportedException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.repository.CommandRepository;
import ru.mtumanov.tm.repository.ProjectRepository;
import ru.mtumanov.tm.repository.TaskRepository;
import ru.mtumanov.tm.repository.UserRepository;
import ru.mtumanov.tm.service.AuthService;
import ru.mtumanov.tm.service.CommandService;
import ru.mtumanov.tm.service.LoggerService;
import ru.mtumanov.tm.service.ProjectService;
import ru.mtumanov.tm.service.ProjectTaskService;
import ru.mtumanov.tm.service.TaskService;
import ru.mtumanov.tm.service.UserService;
import ru.mtumanov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new SystemInfoCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new ApplicationExitCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommnd());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUpdateById());
        registry(new TaskUpdateByIndex());

        registry(new UserChangePasswordCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    public void start(final String[] args) {
        initLogger();
        initDemo();
        processArguments(args);
        while (true) {
            System.out.println("ENTER COMMAND:");
            try {
                final String cmd = TerminalUtil.nextLine();
                processCommand(cmd);
                System.out.println("OK");
                loggerService.command(cmd);
            } catch (final Exception e) {
                System.out.println("FAIL");
                loggerService.error(e);
            }
        }
    }

    private void initDemo() {
        try {
            projectService.add(new Project("Test Project", "Project for test", Status.IN_PROGRESS));
            projectService.add(new Project("Complete Project", "Project with complete status", Status.COMPLETED));
            projectService.add(new Project("FKSKDJ#&$&*@(!!111", "Strange project", Status.NOT_STARTED));
            projectService.add(new Project("11112222", "Project with numbers", Status.IN_PROGRESS));

            taskService.add(new Task("NAME1", "description for task"));
            taskService.add(new Task("NAME2", "description2"));
            taskService.add(new Task("NAME3", "description3"));

            userService.create("COOL_USER", "cool", Role.ADMIN);
            userService.create("NOT_COOL_USER", "Not Cool", "notcool@email.ru");
            userService.create("123qwe!", "vfda4432!", "vfda4432@email.ru");
        } catch (final AbstractException e) {
            System.out.println("ERROR! Fail to nitialized test data.");
            System.out.println(e.getMessage());
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        try {
            processArgument(args[0]);
            System.exit(0);
        } catch (final AbstractException e) {
            System.out.println(e.getMessage());
        }
    }

    private void processArgument(final String arg) throws AbstractException {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    private void processCommand(final String cmd) throws AbstractException {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand abstractCommand = commandService.getCommandByName(cmd);
        if (abstractCommand == null) throw new CommandNotSupportedException(cmd);
        abstractCommand.execute();        
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK_MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        }));
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
