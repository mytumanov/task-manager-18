package ru.mtumanov.tm.command.project;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        getProjectTaskService().removeProjectById(project.getId());
    }
    
}
