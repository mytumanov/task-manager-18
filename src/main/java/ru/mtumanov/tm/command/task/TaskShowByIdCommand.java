package ru.mtumanov.tm.command.task;

import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Show task by id";
    }

    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findOneById(id);
        showTask(task);
    }
    
}
