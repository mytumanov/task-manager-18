package ru.mtumanov.tm.command.system;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Show program version";
    }

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.17.0");
    }
    
}
