package ru.mtumanov.tm.api.repository;

import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.model.Project;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id) throws AbstractEntityNotFoundException;

    Project removeByIndex(Integer index) throws AbstractEntityNotFoundException;

    int getSize();

    void clear();

    boolean existById(String id);

}
