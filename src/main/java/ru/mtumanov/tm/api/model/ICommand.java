package ru.mtumanov.tm.api.model;

import ru.mtumanov.tm.exception.AbstractException;

public interface ICommand {

    String getArgument();

    String getDescription();

    String getName();

    void execute() throws AbstractException;
    
}
