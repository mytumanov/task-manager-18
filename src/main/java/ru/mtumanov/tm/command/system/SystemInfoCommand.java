package ru.mtumanov.tm.command.system;

import ru.mtumanov.tm.command.AbstractCommand;
import ru.mtumanov.tm.util.FormatUtil;

public class SystemInfoCommand extends AbstractCommand {

    @Override
    public String getArgument() {
        return "-i";
    }

    @Override
    public String getDescription() {
        return "Show system info";
    }

    @Override
    public String getName() {
        return "info";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        System.out.println("Usage memory: " + FormatUtil.formatBytes(usageMemory));
    }
    
}
