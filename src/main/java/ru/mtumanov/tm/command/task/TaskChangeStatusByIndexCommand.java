package ru.mtumanov.tm.command.task;

import java.util.Arrays;

import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Change task status by index";
    }

    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STAUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusByIndex(index, status);
    }
    
}
