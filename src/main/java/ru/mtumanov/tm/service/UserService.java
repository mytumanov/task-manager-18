package ru.mtumanov.tm.service;

import java.util.List;

import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.api.service.IUserService;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.EmailEmptyException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.LoginEmptyException;
import ru.mtumanov.tm.exception.field.PasswordEmptyException;
import ru.mtumanov.tm.exception.user.ExistLoginException;
import ru.mtumanov.tm.exception.user.RoleEmptyException;
import ru.mtumanov.tm.exception.user.UserNotFoundException;
import ru.mtumanov.tm.model.User;
import ru.mtumanov.tm.util.HashUtil;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User create(final String login, final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public User add(final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        return userRepository.add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findById(id);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User findByEmail(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = userRepository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public User remove(final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findById(id);
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User removeByEmail(final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = userRepository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return userRepository.remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userRepository.findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User userUpdate(
            final String id,
            final String firstName,
            final String lastName,
            final String middleName
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = userRepository.findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return userRepository.isEmailExist(email);
    }

}
