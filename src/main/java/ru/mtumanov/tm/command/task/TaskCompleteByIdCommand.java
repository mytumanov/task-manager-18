package ru.mtumanov.tm.command.task;

import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Complete task by id";
    }

    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.COMPLETED);
    }
    
}
