package ru.mtumanov.tm.api.model;

import ru.mtumanov.tm.enumerated.Status;

public interface IHaveStatus {

    Status getStatus();

    void setStatus(Status status);

}
