package ru.mtumanov.tm.api.service;

import java.util.List;

import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

public interface IUserService {

    User create(String login, String password) throws AbstractException;

    User create(String login, String password, String email) throws AbstractException;

    User create(String login, String password, Role role) throws AbstractException;

    User add(User user) throws AbstractException;

    List<User> findAll();

    User findById(String id) throws AbstractException;

    User findByLogin(String login) throws AbstractException;

    User findByEmail(String email) throws AbstractException;

    User remove(User user) throws AbstractException;

    User removeById(String id) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    User removeByEmail(String email) throws AbstractException;

    User setPassword(String id, String password) throws AbstractException;

    User userUpdate(String id, String firstName, String lastName, String middleName) throws AbstractException;

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
