package ru.mtumanov.tm.api.repository;

import java.util.List;

import ru.mtumanov.tm.model.User;

public interface IUserRepository {

    User add(User user);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
